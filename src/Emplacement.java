import java.util.Objects;

public class Emplacement {

  int indexRangee;
  int indexEmplacement;
  boolean disponible = true;
  Serveur serveur;

  public Emplacement(int indexRangee, int indexEmplacement) {
    this.indexRangee = indexRangee;
    this.indexEmplacement = indexEmplacement;
  }

  public boolean isDisponible() {
    return disponible && serveur == null;
  }

  public void setDisponible(boolean disponible) {
    this.disponible = disponible;
  }

  public Serveur getServeur() {
    return serveur;
  }

  public void setServeur(Serveur serveur) {
    this.serveur = serveur;
  }

  @Override
  public String toString() {
    return disponible ? Objects.toString(serveur) : "X";
  }

}
