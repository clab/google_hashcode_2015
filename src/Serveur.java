public class Serveur {

	int id;
	int nbEmplacements;
	int capacite;
	Groupe groupe;

	public Serveur(int id, int nbEmplacementsServeur, int capatiteServeur) {
		this.id = id;
		nbEmplacements = nbEmplacementsServeur;
		capacite = capatiteServeur;
	}

	public int getTaille() {
		return nbEmplacements;
	}

	public void setTaille(int taille) {
		this.nbEmplacements = taille;
	}

	public int getCapacite() {
		return capacite;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	@Override
	public String toString() {
		return this.id + "(" + this.getCapacite() + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Serveur other = (Serveur) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public float getRapport() {
		return this.getCapacite() / this.getTaille();
	}
	
}
