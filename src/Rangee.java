import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Rangee {

  int indexRangee;
  List<Emplacement> emplacements;

  public Rangee(int nbEmplacements, int indexRangee) {
    this.indexRangee = indexRangee;
    emplacements = new ArrayList<Emplacement>(nbEmplacements);
    for (int i = 0; i < nbEmplacements; i++) {
      emplacements.add(new Emplacement(indexRangee, i));
    }
  }

  public List<Emplacement> getEmplacement() {
    return emplacements;
  }

  public void setEmplacement(List<Emplacement> emplacements) {
    this.emplacements = emplacements;
  }

  @Override
  public String toString() {
    return "[" + emplacements + "]";
  }

  public boolean positionnerServeur(Serveur serveur) {
    for (Emplacement emplacement : emplacements) {
      if (emplacement.isDisponible()) {
        if (canPositionnerServeurAPartirEmplacement(emplacement, serveur)) {
          positionnerServeurAPartirEmplacement(emplacement, serveur);
          return true;
        }
      }
    }
    return false;
  }

  public boolean canPositionnerServeurAPartirEmplacement(
      Emplacement emplacement, Serveur serveur) {
    int i = 0;
    for (i = emplacement.indexEmplacement; (i < emplacement.indexEmplacement + serveur.nbEmplacements) && i < emplacements.size(); i++) {
      if (!emplacements.get(i).isDisponible()) {
        return false;
      }
    }
    if (i == 100) {
      return false;
    }
    return true;
  }

  private boolean positionnerServeurAPartirEmplacement(
      Emplacement emplacement, Serveur serveur) {
    for (int i = emplacement.indexEmplacement; i < emplacement.indexEmplacement
        + serveur.nbEmplacements; i++) {
      try {
        emplacements.get(i).setServeur(serveur);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    return true;
  }

  public Serveur chercheServeurNotInGroupe() {
    for (Serveur s : getServeurs()) {
      if (s.getGroupe() == null) {
        return s;
      }
    }
    return null;
  }

  public List<Serveur> getServeurs() {
    Set<Serveur> result = new HashSet<Serveur>();
    for (Emplacement emplacement : emplacements) {
      result.add(emplacement.getServeur());
    }
    result.remove(null);
    return new ArrayList<>(result);
  }

  public int calculCapacite() {
    int c = 0;
    for (Serveur serveur : getServeurs()) {
      c += serveur.getCapacite();
    }

    return c;
  }

  public int calculCapaciteGroupe(Groupe groupe) {
    int c = 0;
    for (Serveur serveur : getServeurs()) {
      if (serveur.getGroupe().equals(groupe)) {
        c += serveur.getCapacite();
      }
    }

    return c;
  }

  public Emplacement chercheEmplacement(Serveur serveur) {
    for (Emplacement emp : emplacements) {
      if (serveur == emp.serveur) {
        return emp;
      }
    }

    return null;
  }
}
