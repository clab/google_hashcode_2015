import java.util.ArrayList;
import java.util.List;

public class Table {

  List<Groupe> groupes;
  List<Rangee> rangees;
  List<Serveur> serveursAPositionner;
  int nbServeurs;

  public Table(int nbRangees, int nbEmplacements, int nbServeurs, int nbGroupes) {
    groupes = new ArrayList<>();
    for (int i = 0; i < nbGroupes; i++) {
      groupes.add(new Groupe(i));
    }
    rangees = new ArrayList<Rangee>(nbRangees);
    for (int i = 0; i < nbRangees; i++) {
      rangees.add(new Rangee(nbEmplacements, i));
    }
    this.nbServeurs = nbServeurs;
    serveursAPositionner = new ArrayList<Serveur>(nbServeurs);
  }

  public List<Groupe> getGroupes() {
    return groupes;
  }

  public void setGroupes(List<Groupe> groupes) {
    this.groupes = groupes;
  }

  public List<Rangee> getRangees() {
    return rangees;
  }

  public void setRangees(List<Rangee> rangees) {
    this.rangees = rangees;
  }

  public List<Serveur> getServeursAPositionner() {
    return serveursAPositionner;
  }

  public void setServeursAPositionner(List<Serveur> serveursAPositionner) {
    this.serveursAPositionner = serveursAPositionner;
  }

  @Override
  public String toString() {
    return "Table [rangees=" + rangees + ", serveursAPositionner="
        + serveursAPositionner + "]";
  }

  public boolean positionneServeur(Serveur serveur) {
    for (Rangee rangee : rangees) {
      if (rangee.positionnerServeur(serveur)) {
        return true;
      }
    }
    return false;

  }

  public void calculGroupe() {
    int nbServeursPositionnes = getNbServeursPositionnes();
    while (getNbServeursInGroupes() != nbServeursPositionnes) {
      Groupe groupe = chercheGroupeOptimal();
      for (Rangee rangee : rangees) {
        Serveur serveur = rangee.chercheServeurNotInGroupe();
        if (serveur != null) {
          serveur.setGroupe(groupe);
          groupe.getServeur().add(serveur);
          break;
        }
      }
    }
  }

  private Groupe chercheGroupeOptimal() {
    float min = Float.MAX_VALUE;
    Groupe groupeMin = groupes.get(0);
    for (Groupe groupe : groupes) {
      if (min > groupe.calculCapacite2()) {
        min = groupe.calculCapacite2();
        groupeMin = groupe;
      }
    }
    return groupeMin;
  }

  public int getNbServeursInGroupes() {
    int count = 0;
    for (Groupe groupe : groupes) {
      count += groupe.getServeur().size();
    }
    return count;
  }

  public int getNbServeursPositionnes() {
    int count = 0;
    for (Rangee rangee : rangees) {
      count += rangee.getServeurs().size();
    }
    return count;
  }

  public int calculCapacite() {
    int capaciteGroupes = 0;
    for (Groupe groupe : groupes) {
      capaciteGroupes += groupe.calculCapacite();
    }

    int capaciteRangees = 0;
    for (Rangee rangee : rangees) {
      capaciteRangees += rangee.calculCapacite();
    }

    return 0;
  }

  public Emplacement chercheEmplacement(Serveur serveur) {
    Emplacement emplacement = null;
    for (Rangee rangee : rangees) {
      emplacement = rangee.chercheEmplacement(serveur);
      if (emplacement != null) {
        break;
      }
    }

    return emplacement;
  }
}
