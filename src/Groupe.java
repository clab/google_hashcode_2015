import java.util.ArrayList;
import java.util.List;

public class Groupe {

  int id;
  List<Serveur> serveur = new ArrayList<>();

  public Groupe(int id) {
    this.id = id;
  }

  public List<Serveur> getServeur() {
    return serveur;
  }

  public void setServeur(List<Serveur> serveur) {
    this.serveur = serveur;
  }

  public int calculCapacite() {
    int c = 0;
    for (Serveur serveur : serveur) {
      c += serveur.getCapacite();
    }

    return c;
  }

  public float calculCapacite2() {
    float c = 0;
    int sommeCapacites = 0;

    for (Serveur serveur : serveur) {
      c += (serveur.getCapacite() * serveur.getTaille());
      sommeCapacites += serveur.getCapacite();
    }

    return c / sommeCapacites;
  }

  public int calculCapaciteRangee(Rangee rangee) {
    int c = 0;
    for (Serveur serveur : serveur) {
      c += serveur.getCapacite();
    }

    return c;
  }

  @Override
  public String toString() {
    return "Groupe [id=" + id + ", serveur=" + serveur + "]";
  }
}
