import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

  public static void main(String[] args) throws IOException {
    List<String> lignes = Files
        .readAllLines(Paths
            .get("input/dc.in"));
    String[] configTable = lignes.get(0).split(" ");
    int nbRangees = Integer.valueOf(configTable[0]);
    int nbEmplacements = Integer.valueOf(configTable[1]);
    int nbIndispos = Integer.valueOf(configTable[2]);
    int nbGroupes = Integer.valueOf(configTable[3]);
    int nbServeurs = Integer.valueOf(configTable[4]);

    Table table = new Table(nbRangees, nbEmplacements, nbServeurs,
        nbGroupes);

    for (int i = 1; i < nbIndispos + 1; i++) {
      String[] emplacementIndispo = lignes.get(i).split(" ");
      int indexRangee = Integer.valueOf(emplacementIndispo[0]);
      int indexEmplacement = Integer.valueOf(emplacementIndispo[1]);
      table.getRangees().get(indexRangee).getEmplacement()
          .get(indexEmplacement).setDisponible(false);
    }

    for (int i = nbIndispos + 1; i < lignes.size(); i++) {
      String[] serveur = lignes.get(i).split(" ");
      int nbEmplacementsServeur = Integer.valueOf(serveur[0]);
      int capatiteServeur = Integer.valueOf(serveur[1]);
      table.getServeursAPositionner().add(
          new Serveur(i + 1, nbEmplacementsServeur, capatiteServeur));
    }

    List<Serveur> serveursIntiaux = new ArrayList<Serveur>(
        table.getServeursAPositionner());

    positionnerServeurs(table);
    for (Rangee rangee : table.getRangees()) {
      for (Emplacement emplacement : rangee.getEmplacement()) {
        System.out.print("[" + emplacement.toString() + "]");
      }
      System.out.println();
    }

    table.calculGroupe();
    for (Groupe groupe : table.getGroupes()) {
      System.out.println(groupe.toString());
    }

    ecrireSortie(table, serveursIntiaux);

  }

  private static void ecrireSortie(Table table, List<Serveur> serveursIntiaux)
      throws IOException {
    File file = Files
        .createFile(
            Paths.get("output/dc.out"))
        .toFile();
    FileWriter fw = new FileWriter(file);

    for (Serveur serveur : serveursIntiaux) {
      Emplacement emplacement = table.chercheEmplacement(serveur);
      if (emplacement == null) {
        fw.write('x');
      } else {

        fw.write(emplacement.indexRangee + " "
            + emplacement.indexEmplacement + " "
            + emplacement.serveur.getGroupe().id);
      }

      fw.write("\r\n");

    }
    fw.close();

  }

  private static void positionnerServeurs(Table table) {
    trierServeur(table);
    while (!table.getServeursAPositionner().isEmpty()) {
      Serveur serveur = table.getServeursAPositionner().remove(0);
      if (table.positionneServeur(serveur)) {
        System.out.println("serveur positionne" + serveur);
      } else {
        System.out.println("serveur non positionne" + serveur);
      }
    }
  }

  private static void trierServeur(Table table) {
    Collections.sort(table.getServeursAPositionner(), new Comparator<Serveur>() {

      @Override
      public int compare(Serveur o1, Serveur o2) {
        return o2.getRapport() - o1.getRapport() > 0 ? 1 : o2.getRapport() == o1.getRapport() ? 0 : -1;
      }
    });

  }

}
